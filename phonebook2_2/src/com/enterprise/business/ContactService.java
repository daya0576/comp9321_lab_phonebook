package com.enterprise.business;

import java.util.List;

import com.enterprise.beans.ContactBean;
import com.enterprise.beans.UserBean;
import com.enterprise.business.exception.ContactServiceException;
import com.enterprise.business.exception.UserLoginFailedException;

/**
 * @author $author
 */
public interface ContactService {

	/**
	 * Finds a user with the given details
	 * @param username The username to login with
	 * @param password The password to login with
	 * @return A user if one exists with the given details
	 * @throws UserLoginFailedException When no such user is found
	 */
	UserBean login(String username, String password) throws UserLoginFailedException;
	
	/**
	 * Returns all the contacts for a particular user
	 * @param userId
	 * @return A list of all contacts
	 * @throws ContactServiceException When an error occurs
	 */
	List getContacts(int userId) throws ContactServiceException;
	/**
	 * Adds a record to the contacts
	 * @param bean The contact record to add
	 * @throws ContactServiceException When an error occurs
	 */
	void addRecord(ContactBean bean) throws ContactServiceException;

	/**
	 * Removes a contact record from the database
	 * @param id The id of the record to delete
	 * @return The contact that was deleted from the database
	 * @throws ContactServiceException When an error occurs
	 */
	ContactBean deleteRecord(int id) throws ContactServiceException;

	/**
	 * Finds a contact
	 * @param id
	 * @return
	 */
	ContactBean getContact(int id) throws ContactServiceException;
}
