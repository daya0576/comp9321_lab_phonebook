package com.enterprise.business;

import com.enterprise.common.NestedException;


/**
 * @author yunki
 */
public class ContactServiceException extends NestedException {

	/**
	 * @param message
	 */
	public ContactServiceException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ContactServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public ContactServiceException(Throwable cause) {
		super(cause);
	}

}
