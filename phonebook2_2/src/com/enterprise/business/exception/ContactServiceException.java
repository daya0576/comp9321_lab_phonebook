package com.enterprise.business.exception;

/**
 * @author yunki
 */
public class ContactServiceException extends Exception {

	/**
	 * @param message
	 */
	public ContactServiceException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ContactServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public ContactServiceException(Throwable cause) {
		super(cause);
	}

}
