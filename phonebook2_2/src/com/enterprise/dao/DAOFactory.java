package com.enterprise.dao;

import java.util.HashMap;
import java.util.Map;

import com.enterprise.dao.support.ContactDAOImpl;
import com.enterprise.dao.support.UserDAOImpl;

/**
 *
 * @author  yunki
 */
public class DAOFactory {
	
	private static final String USER_DAO = "userDAO";
	
	private static final String CONTACT_DAO = "contactDAO";
	
	private Map daos;
	
	private static DAOFactory instance = new DAOFactory();
	
	/** Creates a new instance of DAOFactory */
	private DAOFactory() {
		daos = new HashMap();
		daos.put(USER_DAO, new UserDAOImpl());
		daos.put(CONTACT_DAO, new ContactDAOImpl());
	}
	
	/**
	 * Finds the user dao
	 * @return
	 */
	public UserDAO getUserDAO() {
		return (UserDAO) daos.get(USER_DAO);
	}

	/**
	 * Retrieves the contacts dao
	 * @return
	 */
	public ContactDAO getContactDAO() {
		return (ContactDAO) daos.get(CONTACT_DAO);
	}
	
	public static DAOFactory getInstance() {
		return instance;
	}
}
