package com.enterprise.dao;

import com.enterprise.beans.UserBean;


/**
 *
 * @author  yunki
 */
public interface UserDAO {
	
	/**
	 * Tries to locate a user with the given username and password.
	 * 
	 * @param username The username to use to find the user
	 * @param password The password to use to find the user
	 * @return The UserBean if there is a user with the username and password,
	 * null otherwise.
	 * @throws DataAccessException If error occurs while connecting and retrieving
	 * the database
	 */
	UserBean findByLoginDetails(String username, String password) throws DataAccessException;
	
}
