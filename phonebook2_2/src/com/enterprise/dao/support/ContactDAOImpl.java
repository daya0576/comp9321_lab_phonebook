package com.enterprise.dao.support;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.enterprise.beans.ContactBean;
import com.enterprise.common.DBConnectionFactory;
import com.enterprise.common.ServiceLocatorException;
import com.enterprise.dao.DataAccessException;
import com.enterprise.dao.ContactDAO;

/**
 * The Data Access Object for contacts. This object performs the database
 * operations required for maintaining contacts.
 */
public class ContactDAOImpl implements ContactDAO {

	/**
	 * The service locator to retrieve database connections from
	 */
	private DBConnectionFactory services;

	/** Creates a new instance of ContactDAOImpl */
	public ContactDAOImpl() {
		try {
			services = new DBConnectionFactory();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
	}

	public ContactDAOImpl(DBConnectionFactory services) {
		this.services = services;
	}

	public void insert(ContactBean bean) throws DataAccessException {
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"insert into tbl_contacts (owner_id, short_name, full_name, email, contact_number, notes) values (?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, bean.getOwner());
			stmt.setString(2, bean.getShortName());
			stmt.setString(3, bean.getFullName());
			stmt.setString(4, bean.getEmail());
			stmt.setString(5, bean.getContactNumber());
			stmt.setString(6, bean.getNotes());
			
//			System.out.println("=========sql: "+ stmt.toString());
			int n = stmt.executeUpdate();
			stmt.close(); 
			if (n != 1)
				throw new DataAccessException("Did not insert one row into database");
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * @todo Complete this method
	 * @see com.enterprise.dao.ContactDAO#delete(int)
	 */
	public void delete(int id) throws DataAccessException {
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"delete from tbl_contacts where id = ?");
			stmt.setInt(1, id);
			int n = stmt.executeUpdate();
			stmt.close(); 
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * @todo Complete the operation
	 * @see com.enterprise.dao.ContactDAO#findAllForUser(int)
	 */
	public List<ContactBean> findAllByUser(int id) throws DataAccessException {
		List<ContactBean> list = new ArrayList<ContactBean>();
		
		// TODO get the connection
		// TODO set the parameters
		// TODO add each contact bean to the list (hint - use createContactBean
		// method)
		// TODO catch and throw all the exceptions
		// TODO close the connections
		
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from tbl_contacts where owner_id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {	
				ContactBean contact = createContactBean(rs);
				list.add(contact);
			}
			stmt.close(); 
			rs.close();
			
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return list;
	}

	/**
	 * @todo Complete this method
	 * @see com.enterprise.dao.ContactDAO#getContact(int)
	 */
	public ContactBean getContact(int id) throws DataAccessException {

		// TODO get the connection
		// TODO create the prepared statement
		// TODO set the parameters
		// TODO create the contact bean
		// TODO catch and throw all the required parameters
		// TODO close the connections and result sets
		Connection con = null;
		try {
			con = services.createConnection();
			PreparedStatement stmt = con.prepareStatement(
					"select * from TBL_CONTACTS where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				ContactBean contact = createContactBean(rs);
				stmt.close(); 
				return contact;
			}
		} catch (ServiceLocatorException e) {
			throw new DataAccessException("Unable to retrieve connection; " + e.getMessage(), e);
		} catch (SQLException e) {
			throw new DataAccessException("Unable to execute query; " + e.getMessage(), e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		return null;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ContactBean createContactBean(ResultSet rs) throws SQLException {
		ContactBean contact = new ContactBean();
		contact.setEmail(rs.getString("email"));
		contact.setFullName(rs.getString("full_name"));
		contact.setId(rs.getInt("id"));
		contact.setNotes(rs.getString("notes"));
		contact.setOwner(rs.getInt("owner_id"));
		contact.setShortName(rs.getString("short_name"));
		contact.setContactNumber(rs.getString("contact_number"));
		
		return contact;
	}
}
