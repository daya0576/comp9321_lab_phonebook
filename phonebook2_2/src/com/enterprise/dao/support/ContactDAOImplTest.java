/*
 * Created on 10/08/2003
 *
 */
package com.enterprise.dao.support;


import javax.naming.*;
import java.util.List;

import junit.framework.TestCase;

import com.enterprise.beans.ContactBean;
import com.enterprise.beans.UserBean;
import com.enterprise.common.DBConnectionFactory;

/**
 */
public class ContactDAOImplTest extends TestCase {

	ContactDAOImpl contactDAOImpl;
	
	/**
	 * Constructor for ContactDAOImplTest.
	 * @param arg0
	 */
	public ContactDAOImplTest(String arg0) {
		super(arg0);
	}

    public void tearDown() throws Exception {
         // To test JNDI outside Tomcat, we need to set these
         // values manually ... (just for testing purposes)
       System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
           "org.apache.naming.java.javaURLContextFactory");
       System.setProperty(Context.URL_PKG_PREFIXES, 
           "org.apache.naming");            

       // Create InitialContext with java:/comp/env/jdbc
       InitialContext ic = new InitialContext();

       ic.unbind("java:/comp/env/jdbc/HSQLDB");
       
       ic.destroySubcontext("java:/comp/env/jdbc");
       ic.destroySubcontext("java:/comp/env");
       ic.destroySubcontext("java:/comp");
       ic.destroySubcontext("java:");
        
    }
    public void setUp() throws Exception {
         // To test JNDI outside Tomcat, we need to set these
         // values manually ... (just for testing purposes)
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
            "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES, 
            "org.apache.naming");            

        // Create InitialContext with java:/comp/env/jdbc
        InitialContext ic = new InitialContext();

        ic.createSubcontext("java:");
        ic.createSubcontext("java:/comp");
        ic.createSubcontext("java:/comp/env");
        ic.createSubcontext("java:/comp/env/jdbc");
       
        // Construct BasicDataSource reference
        Reference ref = new Reference("javax.sql.DataSource", "org.apache.commons.dbcp.BasicDataSourceFactory", null);
        ref.add(new StringRefAddr("driverClassName", "org.hsqldb.jdbcDriver"));
        
        //TODO: change the following to your own setting
        ref.add(new StringRefAddr("url", "jdbc:hsqldb:F:/UNSW/term1/comp9321/HSQLDB;ifexists=true"));
        ref.add(new StringRefAddr("username", "sa"));
        ref.add(new StringRefAddr("password", ""));
        ic.bind("java:/comp/env/jdbc/HSQLDB", ref);
    
        contactDAOImpl = new ContactDAOImpl(getDBConnectionFactory());
    }

	public void testFindAllByUser() throws Exception {
		// TODO get an implementation of contactdao and use the assertEquals method to test
		// call the method finallAllBy
		ContactDAOImpl contactDAO = new ContactDAOImpl();
		List<ContactBean> list = contactDAO.findAllByUser(1);
		assertNotNull(list);
	}
	
	public void testAddEntry() throws Exception {
		// TODO create a new contactbean and insert it
		ContactDAOImpl contactDAO = new ContactDAOImpl();
		
		ContactBean bean = new ContactBean();
		bean.setContactNumber("0426150925");
		bean.setId(10);
		contactDAO.insert(bean);
		
	}
		

	/**
	 * Note that this method excepts that the database
	 * is loaded with data in the labs. If the data is not present,
	 * then the test will fail. You can comment out this test while 
	 * testing your other methods.
	 * @throws Exception
	 */	
	public void testDeleteEntry() throws Exception {
		ContactDAOImpl contactDAO = new ContactDAOImpl();
		ContactBean p = contactDAO.getContact(10);
		assertNotNull(p);
		contactDAO.delete(5);
		ContactBean q = contactDAO.getContact(10);
		assertNull(q);
	}
	
	public void testGetRecord() throws Exception {
		// TODO gets a contact and checks that the properties
		// are the same as those in the database using
		// assert equals
//		ContactDAOImpl contactDAO = new ContactDAOImpl();
//		ContactBean p = contactDAO.getContact(3);
//		
		UserDAOImpl userDAO = new UserDAOImpl();
		UserBean user = userDAO.findByLoginDetails("helen", "helen");
		assertNotNull(user);
	}
	
	public DBConnectionFactory getDBConnectionFactory() throws Exception {
		DBConnectionFactory factory = new DBConnectionFactory();
		return factory;
	}
}
