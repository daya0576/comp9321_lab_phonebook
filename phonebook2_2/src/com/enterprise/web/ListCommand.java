/*
 * ListCommand.java
 *
 * Created on 9 August 2003, 11:46
 */

package com.enterprise.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.enterprise.beans.ContactBean;
import com.enterprise.beans.UserBean;
import com.enterprise.business.exception.ContactServiceException;
import com.enterprise.web.helper.DelegateFactory;
import com.enterprise.web.helper.ContactDelegate;

/**
 * This command finds all the phone book entries for a particular user
 * unless they are admin
 * @author  yunki
 */
public class ListCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static ContactDelegate phonebookDelegate;
	/** Creates a new instance of ListCommand */
	public ListCommand() {
		phonebookDelegate = DelegateFactory.getInstance().getContactDelegate();			
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserBean user = (UserBean) session.getAttribute("user");
		int id = user.getId();
		
		try {
			List list = phonebookDelegate.getRecords(id);
			request.setAttribute("list", list);
						
			return "/home.jsp";
		} catch (ContactServiceException e) {
			e.printStackTrace();
		}	
		return "/error.jsp";
	}
	
}
