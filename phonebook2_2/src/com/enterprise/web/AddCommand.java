/*
 * AddCommand.java
 *
 * Created on 9 August 2003, 11:20
 */

package com.enterprise.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.enterprise.beans.ContactBean;
import com.enterprise.beans.UserBean;
import com.enterprise.business.ContactService;
import com.enterprise.business.exception.ContactServiceException;
import com.enterprise.web.helper.ContactDelegate;
import com.enterprise.web.helper.DelegateFactory;
import com.sun.corba.se.impl.oa.poa.DelegateImpl;


/**
 * This command adds a new contact entry
 * @author $author 
 */
public class AddCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static ContactDelegate contactDelegate;
	
	/** Creates a new instance of AddCommand */
	public AddCommand() {
		// TODO: use the delegateFactory to get an instance of the contactDelegate
		contactDelegate = DelegateFactory.getInstance().getContactDelegate();	
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {

		// TODO: To add a contact record, you will need to:
		// TODO: get the current user from the HttpSession, their id will be 
		HttpSession session = request.getSession();
		UserBean user = (UserBean) session.getAttribute("user");
		
		// TODO: the "owner" of the new record
		ContactBean contact = new ContactBean();
		int id = user.getId();
		contact.setOwner(id);
		
		// TODO: then get from the request the following parameters:
		// TODO: phone, email, completeName, notes and shortName
		// TODO: and using all these values, populate the properties
		// TODO: of a ContactBean
		contact.setContactNumber(request.getParameter("phone"));
		contact.setEmail(request.getParameter("email"));
		contact.setFullName(request.getParameter("completeName"));
		contact.setNotes(request.getParameter("notes"));
		contact.setShortName(request.getParameter("shortname"));
		
		
		// TODO: then call on the addRecord(contactBean) on the contactDelegate
		try {
			contactDelegate.addRecord(contact);
			
			// TODO: and finally set the attribute "shortName" to be
			// TODO: the shortName of the bean
			// TODO: the page to return if all this is successful is added.jsp
			request.setAttribute("shortName", contact.getShortName());
			
			return "/added.jsp";
		} catch (ContactServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "/error.jsp";
	}
	
}
