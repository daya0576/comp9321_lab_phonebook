/*
 * DelegateFactory.java Created on 12/08/2003
 * 
 * Copyright DiMETRO Software 2003. 
 */
package com.enterprise.web.helper;

import java.util.HashMap;

/**
 * @author yunki
 */
public class DelegateFactory {

	private HashMap delegates = new HashMap();

	private static DelegateFactory instance = new DelegateFactory();

	public static DelegateFactory getInstance() {
		return instance;
	}
		
	private DelegateFactory() {
		delegates.put("Contact", ContactDelegateImpl.getInstance());		
	}

	public ContactDelegate getContactDelegate() {
		return (ContactDelegate) delegates.get("Contact");
	}
}
