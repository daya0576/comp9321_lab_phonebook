/*
 * PhonebookDelegateImpl.java Created on 12/08/2003
 * 
 */
package com.enterprise.web.helper;

import com.enterprise.business.ContactService;
import com.enterprise.business.support.ContactServiceImpl;

/**
 * Simple implementation that just instantiates an instance of
 * the phonebook service - not really ideal
 * @author $author
 */
public class ContactDelegateImpl extends ContactDelegate {

	private static ContactDelegateImpl instance = new ContactDelegateImpl();
	
	private ContactService service;
	
	private ContactDelegateImpl() {
		service = new ContactServiceImpl();
	}
	
	public static ContactDelegate getInstance() {
		return instance;
	}
	/**
	 * @see com.enterprise.web.PhonebookDelegate#getPhonebookService()
	 */
	protected ContactService getService() {
		return service;
	}

}
