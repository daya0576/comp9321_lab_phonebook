/*
 * PhonebookDelegate.java Created on 12/08/2003
 * 
 */
package com.enterprise.web.helper;

import java.util.List;

import com.enterprise.beans.ContactBean;
import com.enterprise.beans.UserBean;
import com.enterprise.business.ContactService;
import com.enterprise.business.exception.ContactServiceException;
import com.enterprise.business.exception.UserLoginFailedException;

/**
 * Abstract business delegate. This class implements all the methods
 * that required by leaves the method getPhonebookService() of the class to 
 * subclasses. 
 * 
 * @author $author
 */
public abstract class ContactDelegate {

    /**
     * Find the phonebook service.
     * @return The business interface PhoneBookService
     */
	protected abstract ContactService getService();

	private ContactService contactService;
	
	/**
	 * Finds a user with the given details
	 * @param username The username to login with
	 * @param password The password to login with
	 * @return A user if one exists with the given details
	 * @throws UserLoginFailedException When no such user is found
	 */
	public UserBean login(String username, String password) throws UserLoginFailedException {
		return getService().login(username, password);
	}
	
	/**
	 * Returns all the records for a particular user
	 * @param userId
	 * @return A list of all contacts
	 * @throws PhonebookServiceException When an error occurs
	 */
	public List getRecords(int userId) throws ContactServiceException {
		return getService().getContacts(userId);
	}

	/**
	 * Adds a contact
	 * @param bean The contact add
	 * @throws ContactServiceException When an error occurs
	 */
	
	public void addRecord(ContactBean bean) throws ContactServiceException {
		getService().addRecord(bean);
	}
	/**
	 * Removes a phonebook record from the database
	 * @param id The id of the record to delete
	 * @throws PhonebookServiceException When an error occurs
	 */
	public void deleteRecord(int id) throws ContactServiceException {
		getService().deleteRecord(id);
	}
	

	/**
	 * @param id
	 * @return
	 */
	public ContactBean getContact(int id) throws ContactServiceException {
		return getService().getContact(id);
	}
	
}
