/*
 * LoginCommand.java
 *
 * Created on 9 August 2003, 11:12
 */

package com.enterprise.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.enterprise.beans.UserBean;
import com.enterprise.business.exception.UserLoginFailedException;
import com.enterprise.web.helper.DelegateFactory;
import com.enterprise.web.helper.ContactDelegate;

/**
 * This is the command that will be used for logging in users.
 * If logon is successful, the command should place a list of phonebook entries
 * in the request attriubutes.
 * @author  yunki
 */
public class LoginCommand implements Command {
	/**
	 * The helper class to delegate all function calls to
	 */
	private static ContactDelegate contactDelegate;	
	/** Creates a new instance of LoginCommand */
	public LoginCommand() {
		contactDelegate = DelegateFactory.getInstance().getContactDelegate();
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
	
		try {
			UserBean user = contactDelegate.login(
				request.getParameter("username"), request.getParameter("password"));
			if (user == null) {
				return "/index.jsp";
			}
			HttpSession session = request.getSession();
			session.setAttribute("user", user);

			// this is not a jsp so it will chain the commands together
			return "list";
		} catch (UserLoginFailedException e) {
			e.printStackTrace();
			return "/loginfailed.jsp";
		}
	}
	
}
