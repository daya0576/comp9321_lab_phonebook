/*
 * DeleteCommand.java
 *
 * Created on 9 August 2003, 11:21
 */

package com.enterprise.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enterprise.beans.ContactBean;
import com.enterprise.business.exception.ContactServiceException;
import com.enterprise.web.helper.ContactDelegate;
import com.enterprise.web.helper.DelegateFactory;

/**
 * This command is to remove a record from the database
 * @author  $author
 */
public class DeleteCommand implements Command {

	/**
	 * The helper class to delegate all function calls to
	 */
	private static ContactDelegate contactDelegate;
		
	/** Creates a new instance of DeleteCommand */
	public DeleteCommand() {
		// TODO: get the contact delegate using the delegate factory
		contactDelegate = DelegateFactory.getInstance().getContactDelegate();	
	}
	
	public String execute(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
	
		// TODO: get the id from the parameters
		int id = Integer.parseInt(request.getParameter("id"));
			
		ContactBean contact = null;
		try {
			// TODO: find the contact bean from the given id
			contact = contactDelegate.getContact(id);
			
			// TODO: call on the delete method from the delegate
			contactDelegate.deleteRecord(id);
			
			request.setAttribute("entry", contact);
			return "/deleted.jsp";
		} catch (ContactServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "/error.jsp";
	}
}
