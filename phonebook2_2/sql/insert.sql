DELETE FROM TBL_CONTACTS;
DELETE FROM TBL_USERS;

insert into TBL_USERS values (1, 'Helen', 'Paik', 1, 'helen', 'helen');
insert into TBL_USERS values (2, 'Yun Ki', 'Lee', 2, 'yunki', 'yunki');

insert into TBL_CONTACTS values (1, 1, 'Brad', 'Brad Pitt', 'brad@pitt.com', '0393841111', 'Not available');
insert into TBL_CONTACTS values (2, 1, 'Jennifer', 'Jennifer Aniston', 'jen@aniston.net', '22230098', 'Has she really moved on?');
insert into TBL_CONTACTS values (3, 2, 'Angelina', 'Angelina Jolie', 'jolie@lara.org', null, 'The first Lara Croft');
