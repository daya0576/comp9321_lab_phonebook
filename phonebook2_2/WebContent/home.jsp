<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.enterprise.beans.*, java.util.*" %>
<%-- <jsp:useBean type="com.enterprise.beans.ContactBean" scope="page"  id="entry1"/> --%>
<jsp:useBean type="com.enterprise.beans.UserBean" scope="session" id="user"/>
<jsp:useBean type="java.util.ArrayList" scope="request" id="list"/>
<html>
<head><title>Welcome to eEnterprise.com</title></head>
<body>
<center>
	<img width="200px" src="images/pg_imagelogo05.jpg"/>
	<h2>Welcome <i><jsp:getProperty name="user" property="lastname"/>
	<jsp:getProperty name="user" property="firstname"/></i></h2>
</center>

<table width="100%" border=1>
<tr>
<th>Short Name</th><TH>Complete Name</TH><TH>Email</TH><TH>Phone</TH><TH>&nbsp;</TH></tr>

<% 
ContactBean entry = null;
for (int i = 0; i < list.size(); i++) { 
    entry = (ContactBean) list.get(i);
%>
	<tr>

		<td><%=entry.getShortName()%></td>
		<Td><%=entry.getFullName()%></Td>
		<Td><%=entry.getEmail()%></Td>
		<Td><%=entry.getContactNumber()%></Td>
		<td><a href='dispatcher?operation=delete&id=<%=entry.getId()%>'>Delete</a></TD>
	</tr>
	<tr>
		<TD><strong>Notes</strong></TD>
		<Td colspan="4"><%=entry.getNotes()%></Td>
	</tr>
<% } %>
</TABLE>

<br><br>
<hr>
<br><br>



To add a new entry please complete the following information:
<p>

<%-- TODO: here create a form that is used to add users.
The action of the form should be dispatcher, method should be post
There should be 4 text input boxes:
	1. shortName of type text
	2. completeName of type text
	3. email of type text
	4. phone of type text
	5. notes of type text (or text area)

To specify the operation "add", you will need to use a hidden input
and finally do not forget the submit button
--%>
<form action='dispatcher?operation=add' method="post">
<table>
    <tr><td>Short Name:</td><td><input name="shortname" type="text"></td></tr>
    <tr><td>Complete Name:</td><td><input name=completeName type="text"></td></tr>
    <tr><td>Contract Number:</td><td><input name="phone" type="text"></td></tr>
    <tr><td>Email:</td><td><input name="email" type="text"></td></tr>
    <tr><td>Notes:</td><td><input rows="3" name="notes" type="textarea"></td></tr>
    <tr><td colspan="2" align="left"><input type="submit" value="add"></TD></TR>
</table>
</form>
<p>
<a href="dispatcher?operation=logout">Logout</a>
</html>

