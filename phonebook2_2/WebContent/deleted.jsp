<%@ page contentType="text/html" import="com.enterprise.beans.ContactBean"%>
<jsp:useBean beanName="entry" type="com.enterprise.beans.ContactBean" scope="request" id="entry"/>

<html>
The entry <jsp:getProperty name="entry" property="shortName"/> has been deleted.
<p>
<a href="dispatcher?operation=list">Return to home</a>
</html>
